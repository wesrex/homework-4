; --------------------------------
; Question 8
; --------------------------------
; fn main() {
;   let h = |i: i32| -> i32 { i + 1 };
;   let y = double_result(h);
; }
;
; fn double_result<F>(y: F) -> i32
;    where F: Fn(i32) -> i32 {
;    2 * y(1)
; }

#lang racket

(module test racket 
  (require "assert.rkt")
  ; Add code below
  ; ------------------
(define i 0)
(define h 0)
(define y 0)

(set! h (+ i 1))
(set! y (double_result y)

((define double_result y i)
 (* 2 y))
  ; ------------------
  ; Add code above
  (assert y 4))